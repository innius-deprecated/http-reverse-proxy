# HTTP Reverse proxy

A simple http reverse proxy to map 80 -> 8080

## Usage:
```docker run -p 80:80 -e SELF=127.0.0.1 quay.io/toincrease/http-reverse-proxy```
