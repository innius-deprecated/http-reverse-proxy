FROM alpine:3.3

MAINTAINER Ron van der Wijngaard <rvdwijngaard@to-increase.com>

ENV NGINX_VERSION nginx-1.9.14
ENV HEADERS_MORE_VERSION 0.29
ENV LUA_VERSION 0.10.2
ENV NGX_DEVEL_KIT_VERSION 0.2.19

ENV LUAJIT_LIB=/usr/lib
ENV LUAJIT_INC=/usr/include/luajit-2.0

RUN apk update && \
    apk add openssl-dev pcre-dev zlib-dev wget build-base lua lua-dev luajit luajit-dev && \
    mkdir -p /tmp/src && \
    cd /tmp/src && \
    wget --no-check-certificate https://github.com/openresty/headers-more-nginx-module/archive/v${HEADERS_MORE_VERSION}.tar.gz && \
    tar -zxvf v${HEADERS_MORE_VERSION}.tar.gz && \
		wget --no-check-certificate https://github.com/openresty/lua-nginx-module/archive/v${LUA_VERSION}.tar.gz && \
		tar -zxvf v${LUA_VERSION}.tar.gz && \
		wget --no-check-certificate https://github.com/simpl/ngx_devel_kit/archive/v${NGX_DEVEL_KIT_VERSION}.tar.gz && \
		tar -zxvf v${NGX_DEVEL_KIT_VERSION}.tar.gz && \
    wget http://nginx.org/download/${NGINX_VERSION}.tar.gz && \
    tar -zxvf ${NGINX_VERSION}.tar.gz && \
    cd /tmp/src/${NGINX_VERSION} && \
    ./configure \
        --with-http_ssl_module \
        --with-http_gzip_static_module \
        --prefix=/opt/nginx \
        --conf-path=/etc/nginx/nginx.conf \
        --add-module=/tmp/src/headers-more-nginx-module-${HEADERS_MORE_VERSION} \
				--add-module=/tmp/src/lua-nginx-module-${LUA_VERSION} \
				--add-module=/tmp/src/ngx_devel_kit-${NGX_DEVEL_KIT_VERSION} \
        --pid-path=/var/run/nginx.pid \
        --http-log-path=/var/log/nginx/access.log \
        --error-log-path=/var/log/nginx/error.log \
        --sbin-path=/usr/sbin/nginx && \
    make && \
    make install && \
    apk del build-base && \
    rm -rf /tmp/src && \
    rm -rf /var/cache/apk/*

ADD nginx.conf /etc/nginx/nginx.conf
ADD cors-options.conf /etc/nginx/cors-options.conf
ADD cache-control.conf /etc/nginx/cache-control.conf
ADD robots.txt /enc/nginx/robots.txt

EXPOSE 80

ENTRYPOINT ["/usr/sbin/nginx"]
CMD ["-c", "/etc/nginx/nginx.conf", "-g", "daemon off;"]
